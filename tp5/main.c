#include "tag.h"

#define PATHNAME ("/home/infoetu/huitelef/Dropbox/cours/asr4/tp5/musique_examples/00000.mp3")

int main(int argc, char const *argv[])
{
	int fd;
	u32 test;

	/* OPEN */
	fd = open(PATHNAME, O_RDONLY);

	read_u32(fd, &test);
	convert_size(test);

	if (fd == -1) {
		perror("open");
		return 1;
	}
	return 0;
}