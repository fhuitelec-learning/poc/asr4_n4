#include "tag.h"

#define SIZE_BUFFER 1

int read_u8 (int fd, u8 *val)
{
	char *buf = malloc(SIZE_BUFFER*sizeof(char));

	/* READ */
	if ( read(fd, buf, SIZE_BUFFER) < 1 )
	{
		perror("read");
		return 0;
	}

	*val = *buf;

	free(buf);

	return 1;
}

int read_u16 (int fd, u16 *val) 
{
	u8 	*unit = malloc(SIZE_BUFFER*sizeof(char)), 
		*unit2 = malloc(SIZE_BUFFER*sizeof(char));

	if (!read_u8(fd, unit))
	{
		perror("read_u8, unit");
		return 0;
	}

	if (!read_u8(fd, unit2))
	{
		perror("read_u8, unit2");
		return 0;
	}

	*val = (*unit2 << 8) + *unit;

	free(unit);
	free(unit2);

	return 1;
}

int read_u32(int fd, u32 *val)
{
	u16	*unit = malloc(SIZE_BUFFER*sizeof(short int)), 
	*unit2 = malloc(SIZE_BUFFER*sizeof(short int));

	if (!read_u16(fd, unit))
	{
		perror("read_u8, unit");
		return 0;
	}

	if (!read_u16(fd, unit2))
	{
		perror("read_u16, unit2");
		return 0;
	}

	*val = (*unit2 << 16) | *unit;

	printf("%8X\n", *val);

	return 1;
}

/* TODO */
u32 convert_size(u32 size)
{
	u32 b0, b1, b2, b3;

	b0 = size & 0x7F
	test = size & 0x7F7F7F7F;
	printf("%8X\n", test);



	return test;
}
