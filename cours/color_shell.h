#include <stdio.h> 

enum {NOIR = 30, ROUGE, VERT, JAUNE, BLEU, MAGENTA, CYAN, BLANC};

int color_equals(char chaine3[], char chaine4[]) {
	int i = 0;
	 
	while(chaine3[i]!='\0')
	{
        if(chaine3[i] != chaine4[i]) {
            return 1;
        }
        i++;
	}
	return 0;
}

int correspondingColor(char texte[]) {
	if (color_equals( texte, "NOIR") == 0 ) {
		return 30;
	} else if ( color_equals(texte, "ROUGE") == 0 ) {
		return 31;
	} else if ( color_equals(texte, "VERT") == 0 ) {
		return 32;
	} else if ( color_equals(texte, "JAUNE") == 0 ) {
		return 33;
	} else if ( color_equals(texte, "BLEU") == 0 ) {
		return 34;
	} else if ( color_equals(texte, "MAGENTA") == 0 ) {
		return 35;
	} else if ( color_equals(texte, "CYAN") == 0 ) {
		return 36;
	} else if ( color_equals(texte, "BLANC") == 0 ) {
		return 37;
	} else {
		return 0;
	}
}

void color(char texte[], char C_texte[]) {
	#ifdef _WIN32
		printf ("%s", texte);
	#else
        printf ("\033[%dm%s\033[0m", correspondingColor(C_texte), texte);
    #endif
}